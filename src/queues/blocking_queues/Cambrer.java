package queues.blocking_queues;

public class Cambrer implements Runnable {
    private Monitor plat;
    private String nom;

    public Cambrer(Monitor plat, String nom) {
        this.plat = plat;
        this.nom = nom;
    }

    @Override
    public void run() {
        for (String m = plat.agafar(); !m.equals("FET!"); m = plat.agafar()) {
            System.out.println(nom + " agafa: " + m);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
    }
}