package queues.blocking_queues;

public class Monitor {
    private String message;
    private boolean empty=true;

    public synchronized String agafar(){
        while(empty){
            try{
                wait();
            }catch(InterruptedException e){}
        }
        empty=true;
        notifyAll();
        //notifyAll(); //per notificar tots els productors
        return message;
    }

    public synchronized void deixar(String m){
        while(!empty){
            try{
                wait();
            }catch(InterruptedException e){}

        }
        empty=false;
        message=m;
        notifyAll(); //notifyAll(); //per notificar tots els consumidors
    }
}