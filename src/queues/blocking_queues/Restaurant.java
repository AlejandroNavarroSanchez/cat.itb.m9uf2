package queues.blocking_queues;

public class Restaurant {
    public static void main(String[] args) {

        Monitor plat = new Monitor();
        Cuiner cui = new Cuiner(plat);
        Cambrer cam1 = new Cambrer(plat, "Cambrer1");
        Cambrer cam2 = new Cambrer(plat, "Cambrer2");
        Thread cuiner = new Thread(cui);
        Thread cambrer1 = new Thread(cam1);
        Thread cambrer2 = new Thread(cam2);
        cuiner.start();
        cambrer1.start();
        cambrer2.start();

    }
}