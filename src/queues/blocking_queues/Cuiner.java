package queues.blocking_queues;

import java.util.Random;

public class Cuiner implements Runnable {
    private Monitor plat;

    public Cuiner(Monitor plat) {
        this.plat = plat;
    }

    @Override
    public void run() {
        String[] text={"Plat 1",
                "Plat 2",
                "Plat 3",
                "Plat 4",
                "Plat 5"};
        for(int i=0;i<text.length;i++){
            Random random = new Random();
            int n = random.nextInt(text.length);
            plat.deixar(text[n]);
            System.out.println("Cuiner deixa el plat: "+text[n]);
            try{
                Thread.sleep(2000);
            }catch(InterruptedException e){}
        }
        plat.deixar("FET!");
    }
}