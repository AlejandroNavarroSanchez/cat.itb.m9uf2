package executors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class ex2 {
    public static void main(String[] args) throws InterruptedException {

        Fila r1 = new Fila();
        Fila r2 = new Fila();
        Fila r3 = new Fila();
        Fila r4 = new Fila();

        List<Fila> rowList = new ArrayList<>(Arrays.asList(r1, r2, r3, r4));
        Taula t = new Taula(rowList);
        t.populateTableWithRowList(rowList);
        t.printTable("bright_yellow");

        ExecutorService es = Executors.newFixedThreadPool(4);

        Future<Integer> f1 = es.submit(r1);
        es.awaitTermination(1, TimeUnit.MILLISECONDS);
        Future<Integer> f2 = es.submit(r2);
        es.awaitTermination(1, TimeUnit.MILLISECONDS);
        Future<Integer> f3 = es.submit(r3);
        es.awaitTermination(1, TimeUnit.MILLISECONDS);
        Future<Integer> f4 = es.submit(r4);

        try {
            f1.get();
            f2.get();
            f3.get();
            f4.get();
            es.shutdown();
        } catch (InterruptedException | ExecutionException ignored) {}
    }
}
