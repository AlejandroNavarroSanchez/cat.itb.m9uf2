package executors;

import java.util.concurrent.Callable;

public class Fila implements Callable<Integer> {
    private final int[] row;

    public Fila() {
        this.row = new int[4];
        fillRow();
    }

    private void fillRow() {
        for (int i = 0; i < row.length; i++) {
            row[i] = (int) ((Math.random() * 9) + 1);
        }
    }

    public int sumaFila() {
        int sum = 0;
        for (int val : row) {
            sum += val;
        }
        String nFila = Thread.currentThread().getName().substring(Thread.currentThread().getName().length() - 1);
        String threadName = Thread.currentThread().getName();
        System.out.println("Fila " + nFila + " (" + threadName + ") : " + sum);
        return sum;
    }

    public int[] getRow() {
        return row;
    }

    @Override
    public Integer call() {
        return sumaFila();
    }
}
