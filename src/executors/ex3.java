package executors;

import java.io.IOException;
import java.util.concurrent.*;

public class ex3 {
    public static void main(String[] args) throws IOException, InterruptedException {
        Arxiu a1 = new Arxiu(".");
        Arxiu a2 = new Arxiu(".");
        Arxiu a3 = new Arxiu(".");

        ExecutorService es = Executors.newFixedThreadPool(3);

        Future<Integer> f1 = es.submit(a1);
        es.awaitTermination(10, TimeUnit.MILLISECONDS); // Unnecessary, just to show it in order.
        Future<Integer> f2 = es.submit(a2);
        es.awaitTermination(10, TimeUnit.MILLISECONDS); // Unnecessary, ...
        Future<Integer> f3 = es.submit(a3);
        es.awaitTermination(10, TimeUnit.MILLISECONDS); // Unnecessary, ...

        try {
            f1.get();
            f2.get();
            f3.get();
            es.shutdown();
        } catch (InterruptedException | ExecutionException ignored) {}
    }
}
