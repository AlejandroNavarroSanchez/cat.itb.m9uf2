package executors;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.Callable;

public class Arxiu implements Callable<Integer> {
    private String[] files;
    private Path path;

    public Arxiu(String p) {
        this.path = Paths.get(p);
        this.files = listFiles(path);
    }

    private String[] listFiles(Path path) {
        File f = new File(String.valueOf(path));
        return f.list();
    }

    private Integer sumaArxius() throws IOException {
        File f = new File(String.valueOf(path));

        int sum = 0;

        assert files != null;
        for (String pathname : files) {
            Path path = Paths.get(pathname);
            long bytes = Files.size(path);
            File file = new File(pathname);
            if (file.isFile())
                sum += bytes;
        }
        String threadName = Thread.currentThread().getName();
        System.out.println("Directory : " + f.getName());
        System.out.println("\t- Sum : " + sum);
        System.out.println("\t- Thread : " + threadName);
        return sum;
    }

    @Override
    public Integer call() throws Exception {
        return sumaArxius();
    }
}
