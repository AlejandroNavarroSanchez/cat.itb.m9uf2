package executors;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;

public class Taula {
    private int[][] matrix;
    // Color format
    public static final String ESC = "\u001b[";
    public static final String RES = "\u001b[0m";
    public static final String black = ESC + "30m";
    public static final String red = ESC + "31m";
    public static final String green = ESC + "32m";
    public static final String yellow = ESC + "33m";
    public static final String blue = ESC + "34m";
    public static final String magenta = ESC + "35m";
    public static final String cyan = ESC + "36m";
    public static final String white = ESC + "37m";
    public static final String bright_black = ESC + "90m";
    public static final String bright_red = ESC + "91m";
    public static final String bright_green = ESC + "92m";
    public static final String bright_yellow = ESC + "93m";
    public static final String bright_blue = ESC + "94m";
    public static final String bright_magenta = ESC + "95m";
    public static final String bright_cyan = ESC + "96m";
    public static final String bright_white = ESC + "97m";

    public Taula(int row, int col) {
        this.matrix = new int[row][col];
    }
    public void populateTableRandomly() {
        for (int i = 0; i < this.matrix.length; i++) {
            for (int j = 0; j < this.matrix[i].length; j++) {
                this.matrix[i][j] = (int) (Math.random() * 9 + 1);
            }
        }
    }

    public Taula (List<Fila> rowList) {
        int rows = rowList.size();
        int cols = rowList.get(0).getRow().length;
        this.matrix = new int[rows][cols];

    }

    public void populateTableWithRowList(List<Fila> rowList) {
        for (int i = 0; i < rowList.size(); i++) {
            for (int j = 0; j < rowList.get(i).getRow().length; j++) {
                this.matrix[i][j] = rowList.get(i).getRow()[j];
            }
        }
    }

    public void printTable(String color) {
        switch (color.toLowerCase(Locale.ROOT)) {
            case "black" -> color = black;
            case "red" -> color = red;
            case "green" -> color = green;
            case "yellow" -> color = yellow;
            case "blue" -> color = blue;
            case "magenta" -> color = magenta;
            case "cyan" -> color = cyan;
            case "white" -> color = white;
            case "bright_black" -> color = bright_black;
            case "bright_red" -> color = bright_red;
            case "bright_green" -> color = bright_green;
            case "bright_yellow" -> color = bright_yellow;
            case "bright_blue" -> color = bright_blue;
            case "bright_magenta" -> color = bright_magenta;
            case "bright_cyan" -> color = bright_cyan;
            case "bright_white" -> color = bright_white;
        }
        for (int[] i : getMatrix()) {
            for (int j : i) {
                System.out.print(color + "[" + RES + j + color + "] " + RES);
            }
            System.out.println();
        }
    }

    public int[][] getMatrix() {
        return this.matrix;
    }

    public void setMatrix(int[][] matrix) {
        this.matrix = matrix;
    }
}
