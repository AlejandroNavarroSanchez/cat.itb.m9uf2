package repas;

public class PrintThread implements Runnable {
    private String str;

    public PrintThread(String str) {
        this.str = str;
    }

    @Override
    public void run() {
        String[] frase = str.split(" ");
        dictar(frase);
    }

    public static synchronized void dictar(String[] frase) {
        for (String paraula : frase) {
            System.out.print(paraula + " ");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println();
    }
}
