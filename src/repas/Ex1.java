package repas;

public class Ex1 {
    public static void main(String[] args) {
        Thread t1=new Thread(new PrintThread("Una vegada hi havia un gat"));
        Thread t2=new Thread(new PrintThread("Once upon a time in the west"));
        Thread t3=new Thread(new PrintThread("En un lugar de la Mancha"));
        t1.start();
        t2.start();
        t3.start();
    }

}
