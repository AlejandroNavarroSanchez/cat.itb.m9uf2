package strategy_pattern;

public class Camisa {
    private String model;
    private String talla;
    private String color;

    public Camisa(String model, String talla, String color) {
        this.model = model;
        this.talla = talla;
        this.color = color;
    }
    // Getters
    public String getModel() {
        return model;
    }
    public String getTalla() {
        return talla;
    }
    public String getColor() {
        return color;
    }
    // Setters
    public void setModel(String model) {
        this.model = model;
    }
    public void setTalla(String talla) {
        this.talla = talla;
    }
    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return String.format("Camisa:\n\t→ Model: %s\n\t→ Talla: %s\n\t→ Color: %s", model, talla, color);
    }
}
