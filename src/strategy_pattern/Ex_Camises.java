package strategy_pattern;

import text_format.TextFormatter;

import java.util.ArrayList;
import java.util.Arrays;

public class Ex_Camises {
    public static void main(String[] args) {
        Camisa c1 = new Camisa("Llarga", "L", "Vermell");
        Camisa c2 = new Camisa("Curta", "S", "Blau");
        Camisa c3 = new Camisa("Llarga", "M", "Verd");
        Camisa c4 = new Camisa("Curta", "XL", "Groc");
        Camisa c5 = new Camisa("Llarga", "L", "Vermell");

        ArrayList<Camisa> camises = new ArrayList<>(Arrays.asList(c1, c2, c3, c4, c5));
        // Camises de color verd
        System.out.println(TextFormatter.BOLD + "Camises de color verd:" + TextFormatter.RESET);
        filtrarCamises(camises, c -> c.getColor().equals("Verd")).forEach(System.out::println);
        System.out.println();
        // Camises talla L
        System.out.println(TextFormatter.BOLD + "Camises talla L:" + TextFormatter.RESET);
        filtrarCamises(camises, c -> c.getTalla().equals("L")).forEach(System.out::println);
        System.out.println();
    }

    private static ArrayList<Camisa> filtrarCamises(ArrayList<Camisa> list, CamisaSeleccionable filtre) {
        ArrayList<Camisa> camises = new ArrayList<>();

        for (Camisa c : list) {
            if (filtre.test(c))
                camises.add(c);
        }

        return camises;
    }
}
