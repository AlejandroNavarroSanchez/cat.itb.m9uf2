package strategy_pattern;

public interface CamisaSeleccionable {
    boolean test(Camisa c);
}
