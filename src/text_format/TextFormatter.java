package text_format;

public class TextFormatter {
    // Reset
    public static final String RESET = "\033[0m";  // Text Reset
    // Regular Colors
    public static final String BLACK = "\033[0;30m";   // BLACK
    public static final String RED = "\033[0;31m";     // RED
    public static final String GREEN = "\033[0;32m";   // GREEN
    public static final String YELLOW = "\033[0;33m";  // YELLOW
    public static final String BLUE = "\033[0;34m";    // BLUE
    public static final String PURPLE = "\033[0;35m";  // PURPLE
    public static final String CYAN = "\033[0;36m";    // CYAN
    public static final String WHITE = "\033[0;37m";   // WHITE
    public static final String BROWN = "\033[38;5;215m";   // BROWN
    // Text Style
    public static final String BOLD = "\u001b[1m"; // BOLD
    public static final String UNDERLINE = "\u001b[4m"; // UNDERLINE
    public static final String REVERSED = "\u001b[7m"; // REVERSED

    public static void parseColor(String color) {
        color = color.toUpperCase();
        switch (color) {
            case "BLACK":
                System.out.print(TextFormatter.BLACK);
                break;
            case "RED":
                System.out.print(TextFormatter.RED);
                break;
            case "GREEN":
                System.out.print(TextFormatter.GREEN);
                break;
            case "YELLOW":
                System.out.print(TextFormatter.YELLOW);
                break;
            case "BLUE":
                System.out.print(TextFormatter.BLUE);
                break;
            case "PURPLE":
                System.out.print(TextFormatter.PURPLE);
                break;
            case "CYAN":
                System.out.print(TextFormatter.CYAN);
                break;
            case "WHITE":
                System.out.print(TextFormatter.WHITE);
                break;
            case "BROWN":
                System.out.println(TextFormatter.BROWN);
            case "BOLD":
                System.out.println(TextFormatter.BOLD);
            case "UNDERLINE":
                System.out.println(TextFormatter.UNDERLINE);
            case "REVERSED":
                System.out.println(TextFormatter.REVERSED);
            default:
                System.out.print(TextFormatter.RESET);
        }
    }

}
