package threads.ex2;

import text_format.TextFormatter;

public class PrimeNumberThread extends Thread {
    long num;

    public PrimeNumberThread(long num) {
        this.num = num;
    }

    private boolean isPrime() {
        boolean flag = false;
        for (int i = 2; i <= num / 2; ++i) {
            // En cas de que no sigui primer
            if (num % i == 0) {
                flag = true;
                break;
            }
        }
        return !flag;
    }

    @Override
    public void run() {
        long n = num;
        n++;
        for (int i = 2; i < n; i++) {
            if(n%i == 0) {
                n++;
                i=2;
            } else {
                continue;
            }
        }
        System.out.println("El següent nombre primer de " + TextFormatter.RED + num + TextFormatter.RESET + " és: " +
                TextFormatter.GREEN + n + TextFormatter.RESET);
    }

    public static void main(String[] args) {
        PrimeNumberThread pnt1 = new PrimeNumberThread(11);
        PrimeNumberThread pnt2 = new PrimeNumberThread(24);
        PrimeNumberThread pnt3 = new PrimeNumberThread(73);
//        pnt1.run();
//        pnt2.run();
//        pnt3.run();
        pnt1.start();
        pnt2.start();
        pnt3.start();
    }
}
