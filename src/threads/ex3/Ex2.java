package threads.ex3;

import text_format.TextFormatter;

public class Ex2 extends Thread{
    @Override
    public void run() {
        super.run();
    }

    public static void main(String[] args) {
        Thread t = new Thread();

        System.out.println("Nom del thread principal: " + TextFormatter.CYAN + Thread.currentThread().getName() + TextFormatter.RESET);
        System.out.println("Nom del meu thread: " + TextFormatter.GREEN + t.getName() + TextFormatter.RESET);
    }
}
