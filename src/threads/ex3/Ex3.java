package threads.ex3;

public class Ex3 {
    public static void main(String[] args) {
        Thread parlar = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("bla bla " + Thread.currentThread().getName());
            }
        });
        parlar.start();
        Runnable menjar = new Runnable() {
            @Override
            public void run() {
                System.out.println("nyam nyam " + Thread.currentThread().getName());
            }
        };
        // menjar.start()
        menjar.run();
    }
}
