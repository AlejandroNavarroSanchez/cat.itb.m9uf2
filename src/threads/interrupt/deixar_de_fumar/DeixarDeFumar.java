package threads.interrupt.deixar_de_fumar;

public class DeixarDeFumar {
    private static Thread t1, t2;
    public static void main(String[] args) {
        Runnable r1 = () -> fumador();
        Runnable r2 = () -> metge();
        t1 = new Thread(r1);
        t2 = new Thread(r2);
        t1.start();
        t2.start();
    }

    private static void metge() {
        try {
            Thread.sleep(3000);
            t1.interrupt();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void fumador() {
        try {
            int cigarrettes = 1;
            while (!t1.isInterrupted()) {
                System.out.println("Fumo cigarreta... " + cigarrettes);
                cigarrettes++;
                Thread.sleep(1); // Afegit per veure-ho a temps real i poder fer saltar l'excepció
            }
        } catch (InterruptedException e) {
            System.out.println("OK. M'acaben d'interrompre.");
            t1.stop(); // Afegit per tallar el bucle
        }
    }
}
