package threads.interrupt.alarma;

import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class Alarma {
    public static Thread t1, t2;
    private static LocalTime sleepTime = LocalTime.now();
    private static LocalTime wakeUpTime = sleepTime.plusSeconds(5);
    private static Duration duration = Duration.between(sleepTime, wakeUpTime);
    public static void main(String[] args) {
        Runnable r1 = () -> comptarOvelles();
        Runnable r2 = () -> despertador();
        t1 = new Thread(r1);
        t2 = new Thread(r2);
        t1.start();
        t2.start();
    }

    private static void despertador() {
        try {
            Thread.sleep(duration.toMillis());
            t1.interrupt();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void comptarOvelles() {
        try {
            int ovelles = 1;
            while (true) {
                System.out.println(ovelles + " ovelles.");
                ovelles++;
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println("Hora d'aixecarse");
            t1.stop();
        }
    }
}
