package threads.ex4;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Stream;

public class Bici extends Thread {
    private String nom;
    private LocalTime inici;
    private int distancia;
    private long temps;

    public Bici(String nom, int distancia, LocalTime inici) {
        this.nom = nom;
        this.inici = inici;
        this.distancia = distancia;
    }
    // Getters
    public String getNom() {
        return nom;
    }
    public LocalTime getInici() {
        return inici;
    }
    public int getDistancia() {
        return distancia;
    }
    public long getTemps() {
        return temps;
    }
    // Setters
    public void setNom(String nom) {
        this.nom = nom;
    }
    public void setInici(LocalTime inici) {
        this.inici = inici;
    }
    public void setDistancia(int distancia) {
        this.distancia = distancia;
    }
    public void setTemps(long temps) {
        this.temps = temps;
    }

    @Override
    public void run() {
        int counter = 0;
        for (int i = 0; i < distancia; i++) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            counter++;
        }
        temps = Duration.between(inici, LocalTime.now()).getNano();
    }

    public static void main(String[] args) throws InterruptedException {
        LocalTime inici = LocalTime.now();
        final int DISTANCIA = 1000;

        Bici b1 = new Bici("Montse", DISTANCIA, inici);
        Bici b2 = new Bici("Fran", DISTANCIA, inici);
        Bici b3 = new Bici("Clara", DISTANCIA, inici);

        b1.start();
        b2.start();
        b3.start();

        b1.join();
        b2.join();
        b3.join();

        System.out.printf("%s ha trigat %d unitats de temps en fer una distancia de %d\n",
                b1.getNom(), b1.getTemps(), b1.getDistancia());
        System.out.printf("%s ha trigat %d unitats de temps en fer una distancia de %d\n",
                b2.getNom(), b2.getTemps(), b2.getDistancia());
        System.out.printf("%s ha trigat %d unitats de temps en fer una distancia de %d\n",
                b3.getNom(), b3.getTemps(), b3.getDistancia());

        ArrayList<Bici> bicis = new ArrayList<>(Arrays.asList(b1,b2,b3));
        // filtrar bici mas rápida
        Bici quickestBike = Collections.min(bicis, Comparator.comparingLong(Bici::getTemps));
        System.out.println("\nLa bici més ràpida és la de: " + quickestBike.getNom());
    }
}
