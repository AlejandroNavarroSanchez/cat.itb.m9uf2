package threads.ex4;

import java.lang.reflect.Array;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class BiciLambda extends Bici implements Runnable{
    public BiciLambda(String nom, int distancia, LocalTime inici) {
        super(nom, distancia, inici);
    }

    public static void main(String[] args) throws InterruptedException {
        Thread b1 = new Thread( () ->  {
            String name = "Montse";
            LocalTime inici = LocalTime.now();
            int distancia = 1000;
            long temps;

            int counter = 0;
            for (int i = 0; i < distancia; i++) {
                try {
                    Thread.currentThread().sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                counter++;
            }
            temps = Duration.between(inici, LocalTime.now()).getNano();

            System.out.printf("%s ha trigat %d unitats de temps en fer una distancia de %d\n",
                    name, temps, distancia);

            Thread.currentThread().setName(String.valueOf(temps));

        });
        Thread b2 = new Thread( () -> {
            String name = "Fran";
            LocalTime inici = LocalTime.now();
            int distancia = 1000;
            long temps;

            int counter = 0;
            for (int i = 0; i < distancia; i++) {
                try {
                    Thread.currentThread().sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                counter++;
            }
            temps = Duration.between(inici, LocalTime.now()).getNano();

            System.out.printf("%s ha trigat %d unitats de temps en fer una distancia de %d\n",
                    name, temps, distancia);

            Thread.currentThread().setName(String.valueOf(temps));
        });
        Thread b3 = new Thread( () -> {
            String name = "Clara";
            LocalTime inici = LocalTime.now();
            int distancia = 1000;
            long temps;

            int counter = 0;
            for (int i = 0; i < distancia; i++) {
                try {
                    Thread.currentThread().sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                counter++;
            }
            temps = Duration.between(inici, LocalTime.now()).getNano();

            System.out.printf("%s ha trigat %d unitats de temps en fer una distancia de %d\n",
                    name, temps, distancia);

            Thread.currentThread().setName(String.valueOf(temps));
        });

        b1.start();
        b2.start();
        b3.start();

        b1.join();
        b2.join();
        b3.join();

        int t1 = Integer.parseInt(b1.getName());
        int t2 = Integer.parseInt(b2.getName());
        int t3 = Integer.parseInt(b3.getName());

        ArrayList<Integer> temps = new ArrayList(Arrays.asList(t1,t2,t3));
        ArrayList<String> noms = new ArrayList<>(Arrays.asList("Montse", "Fran", "Clara"));
        // filtrar bici mas rápida
        int min = temps.get(0);
        String nom = noms.get(0);
        for (int i = 0; i < temps.size(); i++) {
            if (min > temps.get(i)) {
                min = temps.get(i);
                nom = noms.get(i);
            }
        }

        System.out.println("\nLa bici més ràpida és la de: " + nom);
    }
}
