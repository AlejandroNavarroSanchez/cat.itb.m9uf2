package threads.ex4;

import java.lang.reflect.Array;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class BiciRunnable implements Runnable {
    private String nom;
    private LocalTime inici;
    private int distancia;
    private long temps;
    private ArrayList<Bici> bicis;

    public BiciRunnable(String nom, int distancia, LocalTime inici) {
        this.nom = nom;
        this.inici = inici;
        this.distancia = distancia;
    }
    // Getters
    public String getNom() {
        return nom;
    }
    public LocalTime getInici() {
        return inici;
    }
    public int getDistancia() {
        return distancia;
    }
    public long getTemps() {
        return temps;
    }
    // Setters
    public void setNom(String nom) {
        this.nom = nom;
    }
    public void setInici(LocalTime inici) {
        this.inici = inici;
    }
    public void setDistancia(int distancia) {
        this.distancia = distancia;
    }
    public void setTemps(long temps) {
        this.temps = temps;
    }

    @Override
    public void run() {
        int counter = 0;
        for (int i = 0; i < distancia; i++) {
            try {
                Thread.currentThread().sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            counter++;
        }
        temps = Duration.between(inici, LocalTime.now()).getNano();

        System.out.printf("%s ha trigat %d unitats de temps en fer una distancia de %d\n",
                this.getNom(), this.getTemps(), this.getDistancia());

        Thread.currentThread().setName(String.valueOf(temps));
    }

    public static void main(String[] args) throws InterruptedException {
        LocalTime inici = LocalTime.now();
        final int DISTANCIA = 1000;

        Thread b1 = new Thread(new BiciRunnable("Montse", DISTANCIA, inici));
        Thread b2 = new Thread(new BiciRunnable("Fran", DISTANCIA, inici));
        Thread b3 = new Thread(new BiciRunnable("Clara", DISTANCIA, inici));

        b1.start();
        b2.start();
        b3.start();

        b1.join();
        b2.join();
        b3.join();

        int t1 = Integer.parseInt(b1.getName());
        int t2 = Integer.parseInt(b2.getName());
        int t3 = Integer.parseInt(b3.getName());

        ArrayList<Integer> temps = new ArrayList(Arrays.asList(t1,t2,t3));
        ArrayList<String> noms = new ArrayList<>(Arrays.asList("Montse", "Fran", "Clara"));
        // filtrar bici mas rápida
        int min = temps.get(0);
        String nom = noms.get(0);
        for (int i = 0; i < temps.size(); i++) {
            if (min > temps.get(i)) {
                min = temps.get(i);
                nom = noms.get(i);
            }
        }

        System.out.println("\nLa bici més ràpida és la de: " + nom);
    }
}
