package threads.ex1;

import text_format.TextFormatter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FitxerThread extends Thread{
    private String fileName;
    private File file;

    public FitxerThread(String s) {
        fileName = s;
        file = new File(fileName);
    }

    public void readFile() {
        try {
            if (!file.exists()) {
                file.createNewFile();
                BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));
                for (int i = 1; i <= 10; i++) {
                    String line = String.valueOf(i);
                    bw.write(line+"\n");
                }
                bw.close();
            }
            else {
                Scanner sc = new Scanner(file);
                System.out.println();
                int count = 0;
                while (sc.hasNextLine()) {
                    String line = sc.nextLine();
                    count++;
                    System.out.printf("File %s%s%s line #: %d\n", TextFormatter.GREEN, file.getName(), TextFormatter.RESET, count);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        readFile();
    }

    public static void main(String[] args) {
        FitxerThread ft1 = new FitxerThread("F_1.txt");
        FitxerThread ft2 = new FitxerThread("F_2.txt");
        FitxerThread ft3 = new FitxerThread("F_3.txt");
//        ft1.run();
//        ft2.run();
//        ft3.run();
        ft1.start();
        ft2.start();
        ft3.start();
    }
}
