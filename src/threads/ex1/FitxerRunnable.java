package threads.ex1;

import text_format.TextFormatter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FitxerRunnable implements Runnable {
    private String fileName;
    private File file;

    public FitxerRunnable(String s) {
        fileName = s;
        file = new File(fileName);
    }

    public void readFile() {
        try {
            if (!file.exists()) {
                file.createNewFile();
                BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));
                for (int i = 1; i <= 10; i++) {
                    String line = String.valueOf(i);
                    bw.write(line+"\n");
                }
                bw.close();
            }
            else {
                Scanner sc = new Scanner(file);
                System.out.println();
                int count = 0;
                while (sc.hasNextLine()) {
                    String line = sc.nextLine();
                    count++;
                    System.out.printf("File %s%s%s line #: %d\n", TextFormatter.GREEN, file.getName(), TextFormatter.RESET, count);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        readFile();
    }

    public static void main(String[] args) {
        Thread t1 = new Thread(new FitxerRunnable("F_1.txt"));
        Thread t2 = new Thread(new FitxerRunnable("F_2.txt"));
        Thread t3 = new Thread(new FitxerRunnable("F_3.txt"));
//        ft1.run();
//        ft2.run();
//        ft3.run();
        t1.start();
        t2.start();
        t3.start();
    }
}
