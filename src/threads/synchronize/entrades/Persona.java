package threads.synchronize.entrades;

public class Persona {
    private String name;

    public Persona(String name) {
        this.name = name;
    }
    // Getters
    public String getName() {
        return name;
    }
    // Setters
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "name='" + name + '\'' +
                '}';
    }
}
