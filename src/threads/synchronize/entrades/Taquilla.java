package threads.synchronize.entrades;

public class Taquilla {
    private static int entrades = 10;

    public static void reservarEntrades(String name, int nEntrades) {
        synchronized (Taquilla.class) {
            if (nEntrades > 4) {
                System.out.println(name + " no es poden demanar més de 4 entrades.\n");

            } else if (entrades < nEntrades && entrades > 0) {

                System.out.println("\"" + name + "\" vol comprar " + nEntrades + " entrades:");
                System.out.println("\tReserva " + entrades + " entrades.");
                entrades = 0;
                System.out.println("\tQueden " + entrades + " entrades.\n");

            } else if (entrades > nEntrades && entrades > 0) {

                System.out.println("\"" + name + "\" vol comprar " + nEntrades + " entrades:");
                System.out.println("\tReserva " + nEntrades + " entrades.");
                entrades -= nEntrades;
                System.out.println("\tQueden " + entrades + " entrades.\n");

            } else if (entrades == 0) {
                System.out.println("S'han exhaurit les entrades pel concert. Ho sentim \"" + name + "\".\n");
            }
        }
    }

    public static void main(String[] args) {
        Persona p1 = new Persona("Albert");
        Persona p2 = new Persona("Alex");
        Persona p3 = new Persona("Oriol");
        Persona p4 = new Persona("Miquel");
        Persona p5 = new Persona("Jonathan");
        Persona p6 = new Persona("José");
        Persona p7 = new Persona("Jordi");

        Runnable r1 = () -> reservarEntrades(p1.getName(), 4);
        Runnable r2 = () -> reservarEntrades(p2.getName(), 3);
        Runnable r3 = () -> reservarEntrades(p3.getName(), 4);
        Runnable r4 = () -> reservarEntrades(p4.getName(), 5);
        Runnable r5 = () -> reservarEntrades(p5.getName(), 6);
        Runnable r6 = () -> reservarEntrades(p6.getName(), 4);
        Runnable r7 = () -> reservarEntrades(p7.getName(), 1);

        Thread t1 = new Thread(r1);
        Thread t2 = new Thread(r2);
        Thread t3 = new Thread(r3);
        Thread t4 = new Thread(r4);
        Thread t5 = new Thread(r5);
        Thread t6 = new Thread(r6);
        Thread t7 = new Thread(r7);

        System.out.println("Hi ha un total de " + entrades + " entrades.\n");

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();
        t7.start();
    }
}
