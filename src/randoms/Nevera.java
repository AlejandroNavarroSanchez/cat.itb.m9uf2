package randoms;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Nevera {
    private String nom;
    private Integer ous;
    private Double preu;
    private boolean rebaixada;

    public Nevera() {
        generaNevera();
    }

    private void generaNevera() {
        Random rn = new Random();
        String[] noms = {"Beko", "Philips", "Samsung", "LG", "Bosch", "Balay", "Smeg"};
        this.nom = noms[rn.nextInt(noms.length)];
        int nOus = rn.nextInt(24) + 1;
        while (nOus % 6 != 0) {
            nOus = rn.nextInt(24) + 1;
        }
        this.ous = nOus;
        this.preu = ThreadLocalRandom.current().nextDouble(400, 1000);
        this.rebaixada = rn.nextBoolean();
    }

    @Override
    public String toString() {
        DecimalFormat df = new DecimalFormat("###.##");
        return "Nevera{" +
                "nom='" + nom + '\'' +
                ", ous=" + ous +
                ", preu=" + df.format(preu) +
                ", rebaixada=" + rebaixada +
                '}';
    }

    public static void main(String[] args) {
        List<Nevera> neveraList = Arrays.asList(new Nevera(), new Nevera(), new Nevera(), new Nevera(), new Nevera());

        neveraList.forEach(System.out::println);
    }
}